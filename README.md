Kids NutriCheck

Kids NutriCheck is a web application designed to provide nutritional information tailored specifically for children. This tool takes into account age, gender, height, and weight details provided by the user to calculate Body Mass Index (BMI) values. Based on the BMI calculated, the application suggests preferred food items suitable for the user's locality.

In addition to its nutritional functionality, Kids NutriCheck includes an educational component aimed at raising awareness among children about healthy eating habits. This section features a variety of healthy food items as well as junk food items. By selecting either option, users can access animated videos creatively designed to engage children and promote awareness about the importance of making healthy food choices.

Features

- BMI Calculator: Users can input their age, gender, height, and weight details to calculate their BMI values.
- Nutritional Recommendations: Based on the calculated BMI, the application suggests preferred food items suitable for the user's locality.
- Educational Content: The platform offers animated videos creatively designed to educate children about healthy eating habits and the impact of junk food on their health.
- Interactive Interface: The website features an intuitive and user-friendly interface, making it easy for children to navigate and engage with the content.


Usage

1. Navigate to the Kids NutriCheck website.
2. Enter your age, gender, height, weight, and locality details.
3. Click on the "Calculate BMI" button to generate your BMI value.
4. Based on your BMI value, explore the suggested food items for your locality.
5. Visit the educational section to access animated videos about healthy eating habits and junk food awareness.

